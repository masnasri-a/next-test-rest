// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import * as web3 from "@solana/web3.js";
import { GetProgramAccountsFilter } from "@solana/web3.js";
import { TOKEN_PROGRAM_ID } from "@solana/spl-token";

type Data = {
  data: any;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const connection = new web3.Connection(
    web3.clusterApiUrl("devnet"),
    "confirmed"
  );

  const pubkey_string: any = req.query["public_key"];
  const pubkey = new web3.PublicKey(pubkey_string);

  const filters:web3.GetProgramAccountsFilter[] = [
    {
      dataSize: 165,   
    },
    {
      memcmp: {
        offset: 32,    
        bytes: pubkey.toBase58(),  
      }            
    }
 ];

 let accounts = await connection.getParsedProgramAccounts(
    TOKEN_PROGRAM_ID,
    {filters:filters}
 )
 let list_token :any= []
 accounts.forEach((account, _index) => {
    let data:any = account.account.data;
    let mint = data['parsed']['info']['mint'];
    let balance = data['parsed']['info']['tokenAmount']['uiAmount']
    list_token.push({
        "token":mint,
        "balance":balance
    })
        
 })

  res.status(200).json({ data: list_token });
}
