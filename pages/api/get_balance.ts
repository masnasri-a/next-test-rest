// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import * as web3 from "@solana/web3.js";

type Data = {
  balance: number;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const connection = new web3.Connection(
    web3.clusterApiUrl("devnet"),
    "confirmed"
  );
  const pubkey_string: any = req.query["public_key"];
  const pubkey = new web3.PublicKey(pubkey_string);
  let balance = await connection.getBalance(pubkey);
  res.status(200).json({ balance: balance });
}

