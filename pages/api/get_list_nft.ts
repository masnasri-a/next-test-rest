// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import * as web3 from "@solana/web3.js";
import {
    getParsedNftAccountsByOwner,
  } from "@nfteyez/sol-rayz";

type Data = {
  data: any;
};

const get_connection = () => {
    const connection = new web3.Connection(
        web3.clusterApiUrl("devnet"),
        "confirmed"
      );
return connection      
}

const get_nft = async (pubkey:string) => {
    let conn = get_connection()
    let public_key: any = new web3.PublicKey(pubkey)
    const nftArray = await getParsedNftAccountsByOwner({
        publicAddress: public_key,
        connection: conn
    })
    return nftArray
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
    let pk_string:any = (req.query['public_key']);
    let list_nft = await get_nft(pk_string)
    
  res.status(200).json({ data: list_nft });
}
